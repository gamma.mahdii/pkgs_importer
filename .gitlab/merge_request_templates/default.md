## :telescope: Context

_Reasons of why we need this MR_

<!--
Describe the context, situation or problem that led to this MR.
Why do we need this change?

Link the relevant issue if it exists.
-->

## :thinking: What does this MR do and why?

_Summary of what this MR changes_

<!--
Describe in detail what your merge request does.

Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

%{first_multiline_commit}

## :tv: Screenshots or screen recordings

_Screenshots are required for UI changes, and strongly recommended for all other merge requests._

<!--
Please include any relevant screenshots or screen recordings that will assist
reviewers and future readers. If you need help visually verifying the change,
please leave a comment and ping a GitLab reviewer, maintainer, or MR coach.
-->

## :popcorn: How to set up and validate locally

_Numbered steps to set up and validate the change are strongly suggested._

<!--
Example below:

1. With an importer config like this one:
   ```yaml
   import_1:
     source:
   ```
1. Run the tool: `$ go run main.go generate`
1. Now, in a GitLab project (pointed by <project_id>), copy the contents of child_pipeline.yml in a .gitlab-ci.yml file.
1. Start a pipeline and watch the Maven packages being imported.
-->

/assign me
